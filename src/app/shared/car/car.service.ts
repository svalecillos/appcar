import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class CarService {

  public API = '//localhost:8080';
  public CAR_API = this.API + '/cars';

  constructor(private http:  HttpClient) { }

  //Obtiene todos los automoviles
  getAll():Observable<any> {
    return this.http.get(this.API+'/cool-cars');
  }

  //Obtener carro por id
  get(id: string) {
    return this.http.get(this.CAR_API + '/' + id);
  }

  //Guarda el carro
  save(car: any): Observable<any> {
    let result: Observable<Object>;
    if(car['href']){
      console.log(car);
      
      result = this.http.put(car.href, car);
    } else {
      result = this.http.post(this.CAR_API, car);
    }
    return result;
  }

  //Borra el carro
  remove(href: string) {
    return this.http.delete(href);
  }

}
